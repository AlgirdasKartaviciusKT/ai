#REQUIRES:
# pip install -U scikit-learn
# pip install numpy
# pip install spicy
import os
import glob
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import numpy as np
from sklearn.linear_model import SGDClassifier

ratings = ['one_star', 'two_star', 'three_star', 'four_star', 'five_star']
cwd = os.getcwd()
training = []           #training data
training_folders = []   #training folders - SwitchCase
testing = []            #testing data
testing_folders = []    #testing folders - SwitchCase

def switchCase(x):
    return {
        'one_star' : 1,
        'two_star': 2,
        'three_star': 3,
        'four_star': 4,
        'five_star': 5,
    }[x]

def filter(text):
    from nltk.tokenize import word_tokenize
    tokens = word_tokenize(text)
    # convert to lower case
    tokens = [w.lower() for w in tokens]
    # remove punctuation from each word
    import string
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    # remove remaining tokens that are not alphabetic
    words = [word for word in stripped if word.isalpha()]
    # filter out stop words
    from nltk.corpus import stopwords
    stop_words = set(stopwords.words('english'))
    words = [w for w in words if not w in stop_words]
    text = ''
    for i in words:
        text += i +' '
    return text

def analyzeTrainingFile(filename, rating):
    with open(filename, encoding='utf8', errors='ignore') as f:
        text = f.read()
        filtered = filter(text)
        training.extend([filtered])
        folder = switchCase(rating)
        training_folders.extend([folder])
    return text

def analyzeTrainingFiles():
    for rating in ratings:
        filePath = cwd + '\\training\\' + rating
        print(filePath)
        readFiles = glob.glob(os.path.join(filePath, '*.txt'))
        for filename in readFiles:
            analyzeTrainingFile(filename, rating)
    print('Training list. Reading is done..')

def analyzeTestingFile(filename, rating):
    with open(filename, encoding='utf8', errors='ignore') as f:
        text = f.read()
        filtered = filter(text)
        testing.extend([filtered])
        folder = switchCase(rating)
        testing_folders.extend([folder])
    return text

def analyzeTestingFiles():
    for rating in ratings:
        filePath = cwd + '\\data\\' + rating
        print(filePath)
        readFiles = glob.glob(os.path.join(filePath, '*.txt'))
        for filename in readFiles:
            analyzeTestingFile(filename, rating)
    print('Testing list. Reading is done..')

analyzeTrainingFiles()
analyzeTestingFiles()

print("\nTraining files:", len(training))
print("Testing files:", len(testing))

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(training)
X_train_counts.shape

#print(X_train_counts.toarray())
#print(count_vect.get_feature_names())

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape

#KLASIFIKAVIMAS
text_clf_svm = Pipeline([('vect', CountVectorizer()), ('tfidf', TfidfTransformer()),
                         ('clf-svm', SGDClassifier(loss='hinge', penalty='l2', alpha=1e-3, max_iter=5))])
text_clf_svm = text_clf_svm.fit(training, training_folders)
predicted_svm = text_clf_svm.predict(testing)
np.mean(predicted_svm == testing_folders)
print('\nSVM. Classifier done')
print("Accuracy", np.mean(predicted_svm == testing_folders))