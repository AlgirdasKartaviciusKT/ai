import os
import glob
import re
import operator

cwd = os.getcwd()

delimiters = '[- :;,.!?@+()<>\n/_\t=]'

_MODE = 1 # 0 - vieno failo analizei (analizationFile), 1 - visu failu analizei

ratings = ['one_star', 'two_star', 'three_star', 'four_star', 'five_star']

dataFile = 'bin/dictionaryTable.txt'

analizationFile = 'analizationSample.txt'

totalC = 0

ci = {}
pci = {}

def totalWords():
    totalW = 0
    for rating in ratings:
        totalW = totalW + ci[rating]
    return totalW

def addStarCount(arr):
    for rating in ratings:
        ci[rating] += int(arr[rating])

def getCounts(d):
    for key in d:
        addStarCount(d[key])
    
def generateC(d):
    for rating in ratings:
        ci[rating] = 0
    getCounts(d)

def generatePCI(t):
    for rating in ratings:
        pci[rating] = ci[rating]/t

def readData(d):
    with open(dataFile, 'r') as fp:
        line = fp.readline()
        while line:
            line = line[:-1]
            words = line.split('\t')
            if words:
                key = words[0]
                values =  {
                    'one_star' : words[1],
                    'two_star' : words[2],
                    'three_star' : words[3],
                    'four_star' : words[4],
                    'five_star' : words[5]
                }
                d[key] = values
            line = fp.readline()

def calcProbability(d, words, rating):
    pxc = 1
    for word in words:
        if word in d:
            occurences = d[word][rating]
            #print(word + " - " + occurences)
            if occurences == 0:
                probability = 0.1
            else:
                probability = int(occurences)/ci[rating]
        else:
            probability = 0.1
        if probability == 0:
            probability = 0.1
        pxc = pxc * probability
    return pxc * pci[rating]

def sortDictionary(x, des):
    return sorted(x.items(), key=operator.itemgetter(1), reverse=des)

def analyzeSample(d, filename):
    probabilties = {('place_holder', 0.1)}
    with open(filename, encoding='utf8', errors='ignore') as f:
        for line in f.readlines():
            words = list(filter(None, re.split(delimiters,line)))
            if words:
                probabilties = {}
                for rating in ratings:
                    probabilties[rating] = calcProbability(d, words, rating)
                decision = sortDictionary(probabilties, True)[0]
                return decision[0]
            else:
                return 'empty'

def analyzeAll(d):
    total_files = 0
    correct_classifications = 0
    false_classifications = 0
    for rating in ratings:
        path = cwd + '\\data\\' + rating
        print(path)
        for filename in glob.glob(os.path.join(path, '*.txt')):
            print(filename)
            total_files += 1
            des = analyzeSample(d, filename)
            if des == rating:
                correct_classifications += 1
            elif des == 'empty':
                total_files -= 1
            else:
                false_classifications += 1
    print("correct: " + str(correct_classifications) + "/" + str(total_files) + " - " + str(correct_classifications/total_files))
    print("false: " + str(false_classifications) + "/" + str(total_files) + " - " + str(false_classifications/total_files))

def classify():
    d = {}
    print("__reading data")
    readData(d)
    print("__generating Ci")
    generateC(d)
    totalW = totalWords()
    print("** total words: " + str(totalW))
    generatePCI(totalW)
    if _MODE == 0:
        print(analyzeSample(d, analizationFile))
    else:
        analyzeAll(d)

classify()
