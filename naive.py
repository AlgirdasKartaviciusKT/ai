from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob
import re
import os
train = [
    ('I love this sandwich.', '5'),
    ('This is an amazing place!', '5'),
    ('I feel very good about these beers.', '5'),
    ('This is my best work.', '3'),
    ("What an awesome view", '2'),
    ('I do not like this restaurant', '1'),
    ('I am tired of this stuff.', '3'),
    ("I can't deal with this", '2'),
    ('He is my sworn enemy!', '1'),
    ('My boss is horrible.', '4')
]
test = [
    ('The beer was good.', '4'),
    ('I do not enjoy my job', '2'),
    ("I ain't feeling dandy today.", '2'),
    ("I feel amazing!", '2'),
    ('Gary is a friend of mine.', '5'),
    ("I can't believe I'm doing this.", '3')
]

cl = NaiveBayesClassifier(train)

# Classify some text
print(cl.classify("Their burgers are amazing."))  # "pos"
print(cl.classify("I don't like their pizza."))   # "neg"

# Classify a TextBlob
blob = TextBlob("The beer was amazing. But the hangover was horrible. "
                "My boss was not pleased.", classifier=cl)
print(blob)
print(blob.classify())

for sentence in blob.sentences:
    print(sentence)
    print(sentence.classify())

# Compute accuracy
print("Accuracy: {0}".format(cl.accuracy(test)))

# Show 5 most informative features
cl.show_informative_features(5)