I purchased two tablets with the $60 off deal and thought it was a steal. I only realized that they have ads all over the lockscreen and I learned you have to pay $15 to have them removed because I bought them via the special. It's a great tablet but I purchased this thinking I was paying for a tablet not an ad revenue device for Amazon.