import codecs
import re
import nltk
from nltk.stem.lancaster import LancasterStemmer
stemmer = LancasterStemmer()
import os


class Read:

    def filtertext(self, filename, number, result):
        my = []
        ignore_words = ['?']
        file = codecs.open(filename, 'r', 'utf-8')
        lines = file.read()
        file.close()
        words = re.findall(r"[\w]+", lines)
        print(len(words))
        myset = set(words)
        print(len(myset))
        print(str(len(myset) / len(words) * 100) + ' %')
        print(myset)
        # stem and lower each word and remove duplicates
        words = [stemmer.stem(w.lower()) for w in words if w not in ignore_words]
        words = list(set(words))

        print(len(words), words)

        mylist = words
        no_integers = [x for x in mylist if not (x.isdigit()
                                                 or x[0] == '-' and x[1:].isdigit())]
        stars = filename.split('_')
        file = codecs.open(result, 'a+', 'utf-8')
        for i in no_integers:
            file.write(str(i).replace('_', '') + ' ' + str(number[stars[0]]))
            file.write('\n')
        file.close()



reader = Read();
number = {
        'one': 1,
        'two': 2,
        'three': 3,
        'four': 4,
        'five':5}
result = 'words.txt'
if os.path.exists(result):
    os.remove(result)
for filename in os.listdir('duomenys'):

     reader.filtertext(filename, number, result)