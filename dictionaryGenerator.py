import os
import glob
import re
from os import walk
import os, os.path

tdp = 1.0 # percentage of files to read for training

cwd = os.getcwd()
write_path = cwd +'\\bin\dictionaryTable.txt'

enum = ['one_star', 'two_star', 'three_star', 'four_star', 'five_star']
wordCount = {
    'one_star' : 0,
    'two_star' : 0,
    'three_star' : 0,
    'four_star' : 0,
    'five_star' : 0
}

delimiters = '[- :;,.!?@+()<>\n/_\t=]'

def printTable(d):
    with open(write_path, 'w', errors='ignore') as file:
        endline = '\n'
        for key, value in d.items():
            file.write(key + "\t" + str(value['one_star']) + "\t" + str(value['two_star']) + "\t" + str(value['three_star']) + "\t" + str(value['four_star'])+ "\t" + str(value['five_star']))
            file.write(endline)

def readData(dictionary):
    for rating in enum:
        path = cwd + '\\training\\' + rating
        fileList = os.listdir(path) # dir is your directory path
        number_files = len(fileList)
        filesToRead = int(number_files * tdp)
        print(path + ": " + str(filesToRead) + "/" + str(number_files))
        count = 0
        for name in os.listdir(path):
            if count >= filesToRead:
                break
            if os.path.isfile(os.path.join(path, name)):
               filename =  path + '/' +name
               count += 1
               with open(filename, encoding='utf8', errors='ignore') as f:
                   for line in f.readlines():
                       words = list(filter(None, re.split(delimiters, line)))
                       if words:
                           for word in words:
                               wordCount[rating] += 1
                               if word in dictionary:
                                   dictionary[word][rating] += 1
                               else:
                                   value = {
                                       'one_star': 0,
                                       'two_star': 0,
                                       'three_star': 0,
                                       'four_star': 0,
                                       'five_star': 0
                                   }
                                   value[rating] += 1
                                   dictionary[word] = value



        
def generateTable():
    dictionary = {}
    print("__reading data")
    readData(dictionary)
    print("__reading data done")
    print("__generating table")
    printTable(dictionary)
    print("__generating table done")

generateTable()
