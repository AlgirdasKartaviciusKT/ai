#pip install -U textblob nltk
#>python -m textblob.download_corpora
#https://stevenloria.com/simple-text-classification/
from textblob.classifiers import NaiveBayesClassifier
from textblob import TextBlob
import re
import glob
import os
from random import shuffle

import nltk
nltk.download('stopwords')

ratings = ['one_star', 'two_star', 'three_star', 'four_star', 'five_star']
cwd = os.getcwd()
training = []
testing = []
def filter(text):
    from nltk.tokenize import word_tokenize
    tokens = word_tokenize(text)
    # convert to lower case
    tokens = [w.lower() for w in tokens]
    # remove punctuation from each word
    import string
    table = str.maketrans('', '', string.punctuation)
    stripped = [w.translate(table) for w in tokens]
    # remove remaining tokens that are not alphabetic
    words = [word for word in stripped if word.isalpha()]
    # filter out stop words
    from nltk.corpus import stopwords
    stop_words = set(stopwords.words('english'))
    words = [w for w in words if not w in stop_words]
    text = ''
    for i in words:
        text += i +' '
    return text

def analyzeSample(filename, rating):
    with open(filename, encoding='utf8', errors='ignore') as f:
        text = f.read()
        filtered = filter(text)
        training.extend([(filtered, rating)])
    return text
def analyzeAll():
    total_files = 0
    correct_classifications = 0

    cwd = os.getcwd()
    for rating in ratings:
        path = cwd + '\\training\\' + rating
        print(path)
        for filename in glob.glob(os.path.join(path, '*.txt')):
             analyzeSample(filename, rating)

    print('reading done')
def analyzeAllTesting():
    total_files = 0
    correct_classifications = 0

    cwd = os.getcwd()
    for rating in ratings:
        path = cwd + '\\data\\' + rating
        print(path)
        for filename in glob.glob(os.path.join(path, '*.txt')):
             analyzeSampleTesting(filename, rating)

    print('reading done')
def analyzeSampleTesting(filename, rating):

    with open(filename, encoding='utf8', errors='ignore') as f:
        text = f.read()
        testing.extend([(text, rating)])
    return text


def test(maxtesting):

    print(len(testing))
    if (len(testing) < maxtesting):
        maxtesting = len(testing)
    print("Accuracy: {0}".format(cl.accuracy(testing[:maxtesting])))
    print('testing_done')



def check(max):
    for i in testing[:max]:
        textInput = i[0]
        blob = TextBlob(textInput, classifier=cl)
        print(blob)
        print(blob.classify())
def data():
    analyzeAll()
    analyzeAllTesting()
    #mix data
    shuffle(training)
    shuffle(testing)
data()
maxtraining = 2000

print(len(training))
if (len(training) < maxtraining):
    maxtraining = len(training)
cl = NaiveBayesClassifier(training[:maxtraining])
print('training_done')
test(500)
check(20)