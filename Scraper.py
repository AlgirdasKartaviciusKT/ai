import urllib.request
from bs4 import BeautifulSoup as soup
import codecs
import time
import os


cwd = os.getcwd()
#stars = ['&filterByStar=five_star', '&filterByStar=four_star', '&filterByStar=three_star', '&filterByStar=two_star', '&filterByStar=one_star']

def scrapWithBtn(stars, folderName, url, maxPages):
    headers = {}

    headers['User-Agent'] = "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:48.0) Gecko/20100101 Firefox/48.0"
    containers = []
    number = 0
    pageUrl = url.replace('(page)', '1')
    pageUrl = pageUrl.replace('(stars)', stars)
    print(pageUrl)

    pages = maxPage(pageUrl, headers)

    while (len(pages) <= 0):
         pages = maxPage(pageUrl, headers)


    if (len(pages) > 0):
        number = int(str(soup.get_text(pages[len(pages) - 1])).replace(',', ''))

    print(number)
    if (number > maxPages):
        number = maxPages
    print(number)

    for x in range(1, number):
        print(x)
        page = str(x)
        pageUrl = url.replace('(page)', page)
        pageUrl = pageUrl.replace('(stars)', stars)
        pages = request(pageUrl, headers)

        while (len(pages) <= 0):
            pages = request(pageUrl, headers)
        print(len(pages))
        for num in range(0, len(pages)):
            text = soup.get_text(pages[num])
            makeFile(url, text, folderName, page, str(num))


def maxPage(pageUrl, headers):
    req = urllib.request.Request(pageUrl, headers=headers)
    html = urllib.request.urlopen(req).read()
    page_soup = soup(html, "html.parser")
    pages = page_soup.findAll("li", {"class": "page-button"})
    return pages
def request(pageUrl, headers):
    req = urllib.request.Request(pageUrl,
                                 headers=headers)
    html = urllib.request.urlopen(req).read()
    page_soup = soup(html, "html.parser")
    pages = page_soup.findAll("span", {"class": "a-size-base review-text"})
    return pages

def ScrapWithoutOrder( folderName, url, maxPages):
    headers = {}

    headers['User-Agent'] = "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:48.0) Gecko/20100101 Firefox/48.0"
    containers = []
    number = 0
    pageUrl = url.replace('(page)', '1')

    print(pageUrl)

    pages = maxPage(pageUrl, headers)

    while (len(pages) <= 0):
         pages = maxPage(pageUrl, headers)


    if (len(pages) > 0):
        number = int(str(soup.get_text(pages[len(pages) - 1])).replace(',', ''))

    print(number)
    if (number > maxPages):
        number = maxPages
    print(number)

    for x in range(1, number):
        print(x)
        page = str(x)
        pageUrl = url.replace('(page)', page)

        pages = request(pageUrl, headers)

        while (len(pages) <= 0):
            pages = request(pageUrl, headers)
        print(len(pages))
        for num in range(0, len(pages)):
            text = soup.get_text(pages[num])
            makeFile(url, text, folderName, page, str(num))
def makeFile(url ,text, folder, page, number):
    cwd = os.getcwd()
    cwd = cwd + '/training/'
    print(cwd)
    path = cwd + folder
    if (os.path.exists(path) == False):
        os.mkdir(path)
    words = url.split('/')
    path = path +'/'+ words[3]+'_'+page +'_'+number +'.txt'
    print(path)
    file = codecs.open(path, 'w', 'utf-8')
    file.write(text)

    file.close()
    return path


url1 = 'https://www.amazon.com/All-New-Amazon-Echo-Dot-Add-Alexa-To-Any-Room/product-reviews/B01DFKC2SO/ref=cm_cr_othr_d_paging_btm_(page)?ie=UTF8&reviewerType=all_reviews&(stars)&pageNumber=(page)'
url2 = 'https://www.amazon.com/Fire-Tablet-Alexa-Display-Black/product-reviews/B01J94SWWU/ref=cm_cr_othr_d_paging_btm_(page)?ie=UTF8&reviewerType=all_reviews&pageNumber=(page)&(stars)'
url3 = 'https://www.amazon.com/Wrangler-Authentics-Mens-Classic-Relaxed/product-reviews/B00XKXHLGA/ref=cm_cr_othr_d_paging_btm_(page)?ie=UTF8&reviewerType=all_reviews(stars)&pageNumber=(page)'
url4 = 'https://www.amazon.com/Cards-Against-Humanity-LLC-CAHUS/product-reviews/B004S8F7QM/ref=cm_cr_othr_d_paging_btm_(page)?ie=UTF8&reviewerType=all_reviews(stars)&pageNumber=(page)'

urlWithoutOrder = 'https://www.amazon.com/Plugable-Bluetooth-Adapter-Raspberry-Compatible/product-reviews/B009ZIILLI/ref=cm_cr_arp_d_paging_btm_2?ie=UTF8&reviewerType=all_reviews&pageNumber=2'
#urlR = 'https://www.amazon.com/Fire-Tablet-Alexa-Display-Black/product-reviews/B01J94SWWU/ref=cm_cr_arp_d_paging_btm_2?ie=UTF8&reviewerType=all_reviews&pageNumber='

stars = ['&filterByStar=five_star', '&filterByStar=four_star', '&filterByStar=three_star', '&filterByStar=two_star', '&filterByStar=one_star']
#scrapRandom(urlR)
maxPages = 10
#ScrapWithoutOrder('all', urlWithoutOrder, maxPages)
for star in stars:
    text = star.split('=')

    folderName = text[1]

    scrapWithBtn(star, folderName, url1, maxPages)
    scrapWithBtn(star, folderName, url2, maxPages)
    scrapWithBtn(star, folderName, url3, maxPages)
    scrapWithBtn(star, folderName, url4, maxPages)





